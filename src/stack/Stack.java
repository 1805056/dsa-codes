package stack;

public class Stack implements StackMethods {

    private final int[] array;
    private int top;

    public Stack() {
        int STACK_MAX_SIZE = 100;
        array = new int[STACK_MAX_SIZE];
    }

    public Stack( int size ) {
        array = new int[size];
    }

    @Override
    public void push( int data ) {

        if (top > array.length - 1) {
            throw new RuntimeException("Stack Overflow");
        }

        array[top] = data;
        top++;
    }

    @Override
    public int pop() {
        if (top < 0)
            throw new RuntimeException("Stack Underflow");

        top--;
        return array[top];
    }

    @Override
    public int peek() {
        if (top > 0)
            return array[top - 1];
        else
            throw new RuntimeException("No elements in stack");
    }

    @Override
    public int size() {
        return top;
    }

    @Override
    public boolean isEmpty() {
        return top <= 0;
    }
}
