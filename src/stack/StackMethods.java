package stack;

public interface StackMethods{
    void push(int data);
    int pop();
    int peek();
    int size();
    boolean isEmpty();
}
