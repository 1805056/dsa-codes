package knapSack;

/**
 * Created by Amimul Ehsan
 * Date: 8/26/2021
 * Time: 8:53 PM
 */

public class ZeroOneKnapSack {
    private static int zeroOneKnapSack( int[] weight, int[] value, int totalCapacity ) {

        int[][] matrix = new int[weight.length + 1][totalCapacity + 1];
        int N = weight.length;

        for (int itemNumber = 0; itemNumber <= N; itemNumber++) {
            for (int currentWeight = 0; currentWeight <= totalCapacity; currentWeight++) {

                if (itemNumber == 0 || currentWeight == 0)
                    matrix[itemNumber][currentWeight] = 0;
                else if (weight[itemNumber - 1] <= currentWeight)
                    matrix[itemNumber][currentWeight] = Math.max(
                            matrix[itemNumber - 1][currentWeight - weight[itemNumber - 1]] + value[itemNumber - 1],
                            matrix[itemNumber - 1][currentWeight]);
                else
                    matrix[itemNumber][currentWeight] = matrix[itemNumber - 1][currentWeight];
            }
        }

        return matrix[weight.length][totalCapacity];
    }

    public static void main( String[] args ) {
        int[] value = new int[]{7, 8, 12, 6, 10};
        int[] weight = new int[]{2, 1, 2, 2, 2};
        int totalCapacity = 6;
        System.out.println("Total profit can be earned: " + zeroOneKnapSack(weight, value, totalCapacity));
    }
}
