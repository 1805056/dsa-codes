package knapSack;

import java.util.Arrays;

/**
 * Created by Amimul Ehsan
 * Date: 8/26/2021
 * Time: 8:19 PM
 */

class Item {

    double weight;
    double value;
    double density;

    Item( double weight, double value, double density ) {
        this.weight = weight;
        this.value = value;
        this.density = density;
    }
}

public class FractionalKnapSack {

    static double getMaxValue( double[] weight, double[] value, double capacity ) {
        int size = weight.length;
        Item[] itemArray = new Item[size];

        for (int i = 0; i < size; i++)
            itemArray[i] = new Item(weight[i], value[i], value[i] / weight[i]);

        Arrays.sort(itemArray, ( o1, o2 ) -> (int) (o2.density - o1.density));

        int i = 0;
        double totalProfit = 0;

        while (capacity > 0) {
            if (itemArray[i].weight <= capacity) {
                capacity = capacity - itemArray[i].weight;
                totalProfit += itemArray[i].value;
                i++;
            } else {
                totalProfit = totalProfit +
                        capacity * (itemArray[i].value / itemArray[i].weight);
                capacity = 0;
                i++;
            }
        }

        return totalProfit;
    }


    public static void main( String[] args ) {
        double[] weight = {10, 40, 20, 30};
        double[] value = {60, 40, 100, 120};
        int capacity = 50;
        System.out.println(getMaxValue(weight, value, capacity));
    }
}