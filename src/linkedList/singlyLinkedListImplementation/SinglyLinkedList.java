package linkedList.singlyLinkedListImplementation;

public class SinglyLinkedList<T> implements LinkedList<T> {

    private int size;
    private Node<T> headNode;
    private Node<T> tailNode;

    public SinglyLinkedList() {

    }

    @Override
    public void addFirst( T data ) {

        Node<T> newNode = new Node<>(data, null);

        if (headNode == null) {
            headNode = newNode;
            tailNode = newNode;
        } else {
            newNode.setNextNode(headNode);
            headNode = newNode;
        }

        size++;
    }

    @Override
    public void addLast( T data ) {

        Node<T> newNode = new Node<>(data, null);

        if (headNode == null) {
            headNode = newNode;
        } else {
            tailNode.setNextNode(newNode);
        }

        tailNode = newNode;
        size++;
    }

    public void printNodes() {

        Node<T> currentNode = headNode;

        while (currentNode != tailNode) {
            System.out.println("Data : " + currentNode.getData());
            currentNode = currentNode.getNextNode();
        }

        System.out.println("Data : " + tailNode.getData());
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T first() {
        return headNode.getData();
    }

    @Override
    public T last() {
        return tailNode.getData();
    }

    @Override
    public void removeFirst() {
        if (size == 0)
            throw new RuntimeException("No element to remove");
        else {
            headNode = headNode.getNextNode();
            size--;
        }
    }
}
