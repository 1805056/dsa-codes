package linkedList.singlyLinkedListImplementation;

public class Test {
    public static void main( String[] args ) {

        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();

        singlyLinkedList.addLast(10);
        singlyLinkedList.addLast(20);
        singlyLinkedList.addLast(30);
        singlyLinkedList.addLast(40);
        singlyLinkedList.addLast(50);
        singlyLinkedList.addLast(60);
        singlyLinkedList.addLast(70);
        singlyLinkedList.addLast(80);

        singlyLinkedList.addFirst(5);

        System.out.println(singlyLinkedList.size());

        singlyLinkedList.removeFirst();

        singlyLinkedList.printNodes();

        System.out.println(singlyLinkedList.size());
        System.out.println(singlyLinkedList.first());
        System.out.println(singlyLinkedList.last());


        System.out.println("________________________________________Checking for string____________________________________________________");


        SinglyLinkedList<String> singlyLinkedList1 = new SinglyLinkedList<>();

        singlyLinkedList1.addLast("Name");
        singlyLinkedList1.addLast("Is");
        singlyLinkedList1.addLast("Amimul");
        singlyLinkedList1.addLast("Ehsan");
        singlyLinkedList1.addLast("Rahi");

        singlyLinkedList1.addFirst("My");

        singlyLinkedList1.printNodes();

    }
}
