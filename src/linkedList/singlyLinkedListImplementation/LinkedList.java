package linkedList.singlyLinkedListImplementation;

public interface LinkedList<T> {
    void addFirst( T data );

    void addLast( T data );

    int size();

    boolean isEmpty();

    T first();

    T last();

    void removeFirst();
}
