package linkedList.problems;

public class ListNode<T> {
    private final T data;
    private ListNode<T> nextNode;


    public ListNode( T data, ListNode<T> nextNode ) {
        this.data = data;
        this.nextNode = nextNode;
    }

    public T getData() {
        return data;
    }

    public ListNode<T> getNextNode() {
        return nextNode;
    }

    public void setNextNode( ListNode<T> nextNode ) {
        this.nextNode = nextNode;
    }
}
