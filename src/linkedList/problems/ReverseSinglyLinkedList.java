package linkedList.problems;

import linkedList.singlyLinkedListImplementation.Node;

//Reverse a singly linked list without using any data structure
public class ReverseSinglyLinkedList {
    public static void main( String[] args ) {
        SinglyLinkedList<Integer> testLinkList = new SinglyLinkedList<>();
        addData(testLinkList);
        testLinkList.reverseLinkList();
        testLinkList.printNodes();
    }

    private static void addData( SinglyLinkedList<Integer> testLinkList ) {
        testLinkList.addLast(1);
        testLinkList.addLast(2);
        testLinkList.addLast(3);
        testLinkList.addLast(4);
        testLinkList.addLast(5);
        testLinkList.addLast(6);
        testLinkList.addLast(7);
        testLinkList.addLast(8);
        testLinkList.addLast(9);
        testLinkList.addLast(10);
    }
}


class SinglyLinkedList<T> implements linkedList.singlyLinkedListImplementation.LinkedList<T> {

    private int size;
    private Node<T> headNode;
    private Node<T> tailNode;

    public SinglyLinkedList() {

    }

    @Override
    public void addFirst( T data ) {

        Node<T> newNode = new Node<>(data, null);

        if (headNode == null) {
            headNode = newNode;
            tailNode = newNode;
        } else {
            newNode.setNextNode(headNode);
            headNode = newNode;
        }

        size++;
    }

    @Override
    public void addLast( T data ) {

        Node<T> newNode = new Node<>(data, null);

        if (headNode == null) {
            headNode = newNode;
        } else {
            tailNode.setNextNode(newNode);
        }

        tailNode = newNode;
        size++;
    }

    public void printNodes() {

        Node<T> currentNode = headNode;

        while (currentNode.getNextNode() != null) {
            System.out.println("Data : " + currentNode.getData());
            currentNode = currentNode.getNextNode();
        }

        System.out.println("Data : " + currentNode.getData());
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T first() {
        return headNode.getData();
    }

    @Override
    public T last() {
        return tailNode.getData();
    }

    @Override
    public void removeFirst() {
        if (size == 0)
            throw new RuntimeException("No element to remove");
        else {
            headNode = headNode.getNextNode();
            size--;
        }
    }

    public void reverseLinkList() {

        Node<T> prevNode = null;
        Node<T> currentNode = headNode;
        Node<T> nextNode = headNode.getNextNode();

        while (nextNode != null) {
            currentNode.setNextNode(prevNode);
            prevNode = currentNode;
            currentNode = nextNode;
            nextNode = currentNode.getNextNode();
        }

        currentNode.setNextNode(prevNode);
        headNode = currentNode;
    }
}

