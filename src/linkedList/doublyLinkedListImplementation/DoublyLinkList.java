package linkedList.doublyLinkedListImplementation;

public class DoublyLinkList<T> implements LinkedList<T> {

    private int size;
    private Node<T> headNode;

    @Override
    public void addFirst( T data ) {

        Node<T> newNode = new Node<>(data, null, null);

        if (headNode != null) {
            newNode.setNextNode(headNode);
            headNode.setPreviousNode(newNode);
        }

        headNode = newNode;
        size++;
    }

    @Override
    public void addLast( T data ) {
        Node<T> newNode = new Node<>(data, null, null);

        if (headNode == null) {
            headNode = newNode;
        } else {
            Node<T> tempNode = headNode;

            while (tempNode.getNextNode() != null) {
                tempNode = tempNode.getNextNode();
            }

            newNode.setPreviousNode(tempNode);
            tempNode.setNextNode(newNode);
        }

        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T first() {
        return headNode.getData();
    }

    @Override
    public T last() {
        Node<T> tempNode = headNode;

        while (tempNode.getNextNode() != null) {
            tempNode = tempNode.getNextNode();
        }

        return tempNode.getData();
    }

    @Override
    public void removeFirst() {
        if (size == 0)
            throw new RuntimeException("No elements to remove");
        else {
            headNode = headNode.getNextNode();
            headNode.setPreviousNode(null);
            size--;
        }
    }

    @Override
    public void removeLast() {
        if (size == 0) {
            throw new RuntimeException("No elements to be removed");
        } else {
            Node<T> tempNode = headNode;

            while (tempNode.getNextNode() != null) {
                tempNode = tempNode.getNextNode();
            }

            tempNode.getPreviousNode().setNextNode(null);
            size--;
        }
    }

    public void printList() {
        Node<T> tempNode = headNode;

        while (tempNode.getNextNode() != null) {
            System.out.println("Data: " + tempNode.getData());
            tempNode = tempNode.getNextNode();
        }

        System.out.println("Data: " + tempNode.getData());
    }
}
