package linkedList.doublyLinkedListImplementation;

public class Test {
    public static void main( String[] args ) {
        DoublyLinkList<Integer> testList = new DoublyLinkList<>();

        testList.addLast(5);
        testList.addLast(6);
        testList.addLast(7);
        testList.addLast(8);
        testList.addLast(9);

        testList.printList();
        testList.removeLast();
        System.out.println("\n");
        testList.printList();
    }
}
