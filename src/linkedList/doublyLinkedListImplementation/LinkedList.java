package linkedList.doublyLinkedListImplementation;

public interface LinkedList<T> {
    void addFirst( T data );

    void addLast( T data );

    int size();

    boolean isEmpty();

    T first();

    T last();

    void removeFirst();

    void removeLast();
}
