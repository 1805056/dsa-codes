package Heap;

/**
 * Created by Amimul Ehsan
 * Date: 8/23/2021
 * Time: 8:28 PM
 */

public class MaxHeap {

    private int HEAP_MAX_SIZE = 100;
    private int size = 1;
    private final int[] heapArray;

    public MaxHeap() {
        heapArray = new int[HEAP_MAX_SIZE];
        heapArray[0] = Integer.MAX_VALUE;
    }

    public MaxHeap( int heapSize ) {
        heapArray = new int[heapSize];
        HEAP_MAX_SIZE = heapSize;
        heapArray[0] = Integer.MAX_VALUE;
    }

    private int getLeftChildIndex( int parentIndex ) {
        return 2 * parentIndex;
    }

    private int getRightChildIndex( int parentIndex ) {
        return 2 * parentIndex + 1;
    }

    private int getParentIndex( int childIndex ) {
        return childIndex / 2;
    }

    public int getHeapSize() {
        return size - 1;
    }

    private void swapElement( int index1, int index2 ) {
        int temp = heapArray[index1];
        heapArray[index1] = heapArray[index2];
        heapArray[index2] = temp;
    }

    private void shiftUp( int index ) {

        int parentIndex = getParentIndex(index);

        while (index > 0 && heapArray[parentIndex] < heapArray[index]) {
            swapElement(parentIndex, index);
            index = parentIndex;
            parentIndex = getParentIndex(index);
        }
    }

    public void insert( int data ) {
        if (size >= HEAP_MAX_SIZE) {
            System.out.println("Can not insert element, heap is full");
        } else {
            heapArray[size] = data;
            shiftUp(size);
            size++;
        }
    }

    public void printHeap() {
        for (int i = 1; i < size; i++)
            System.out.println(heapArray[i]);

        System.out.println("Heap size : " + getHeapSize());
    }

    public int remove() {
        if (getHeapSize() <= 0)
            throw new RuntimeException("No elements in the heap");
        else {
            int element = heapArray[1];
            heapArray[1] = heapArray[size - 1];
            shiftDown(1);
            size--;
            return element;
        }
    }

    private void shiftDown( int index ) {
        do {
            int largestIndex = index;
            int leftChildIndex = getLeftChildIndex(index);
            int rightChildIndex = getRightChildIndex(index);

            if (heapArray[largestIndex] < heapArray[leftChildIndex])
                largestIndex = leftChildIndex;
            else
                largestIndex = rightChildIndex;

            if (largestIndex == index)
                break;
            else {
                swapElement(index, largestIndex);
                index = largestIndex;
            }
        } while (index < size);
    }

    public static void main( String[] args ) {
        MaxHeap maxHeap = new MaxHeap();

        maxHeap.insert(5);
        maxHeap.insert(3);
        maxHeap.insert(17);
        maxHeap.insert(10);
        maxHeap.insert(84);
        maxHeap.insert(19);
        maxHeap.insert(6);
        maxHeap.insert(22);
        maxHeap.insert(9);

        maxHeap.printHeap();
        maxHeap.remove();
        maxHeap.printHeap();
    }

}
