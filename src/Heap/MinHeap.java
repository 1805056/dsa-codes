package Heap;

/**
 * Created by Amimul Ehsan
 * Date: 8/23/2021
 * Time: 9:13 PM
 */

public class MinHeap {
    private int HEAP_MAX_SIZE = 100;
    private int size = 1;
    private final int[] heapArray;

    public MinHeap() {
        heapArray = new int[HEAP_MAX_SIZE];
        heapArray[0] = Integer.MIN_VALUE;
    }

    public MinHeap( int heapSize ) {
        heapArray = new int[heapSize];
        HEAP_MAX_SIZE = heapSize;
        heapArray[0] = Integer.MIN_VALUE;
    }

    private int getLeftChildIndex( int parentIndex ) {
        return 2 * parentIndex;
    }

    private int getRightChildIndex( int parentIndex ) {
        return 2 * parentIndex + 1;
    }

    private int getParentIndex( int childIndex ) {
        return childIndex / 2;
    }

    public int getHeapSize() {
        return size;
    }

    private void swapElement( int index1, int index2 ) {
        int temp = heapArray[index1];
        heapArray[index1] = heapArray[index2];
        heapArray[index2] = temp;
    }

    private void shiftUp( int index ) {

        int parentIndex = getParentIndex(index);

        while (index > 0 && heapArray[parentIndex] > heapArray[index]) {
            swapElement(parentIndex, index);
            index = parentIndex;
            parentIndex = getParentIndex(index);
        }
    }

    public void insert( int data ) {
        if (size >= HEAP_MAX_SIZE) {
            System.out.println("Can not insert element, heap is full");
        } else {
            heapArray[size] = data;
            shiftUp(size);
            size++;
        }
    }

    public void printHeap() {
        for (int i = 1; i < size; i++)
            System.out.println(heapArray[i]);

        System.out.println("Heap size : " + getHeapSize());
    }

    public static void main( String[] args ) {
        MinHeap minHeap = new MinHeap();

        minHeap.insert(5);
        minHeap.insert(3);
        minHeap.insert(17);
        minHeap.insert(10);
        minHeap.insert(84);
        minHeap.insert(19);
        minHeap.insert(6);
        minHeap.insert(22);
        minHeap.insert(9);

        minHeap.printHeap();
    }
}
