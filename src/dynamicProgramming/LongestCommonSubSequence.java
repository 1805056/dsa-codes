package dynamicProgramming;

/**
 * Created by Amimul Ehsan
 * Date: 8/27/2021
 * Time: 5:31 PM
 */

public class LongestCommonSubSequence {

    public static int longestCommonSubsequence( String text1, String text2 ) {
        int size1 = text1.length();
        int size2 = text2.length();

        int[][] dpTable = new int[size1 + 1][size2 + 1];
        char[] charArray1 = text1.toCharArray();
        char[] charArray2 = text2.toCharArray();

        for (int i = 0; i <= size1; i++) {
            for (int j = 0; j <= size2; j++) {
                if (i == 0 || j == 0)
                    dpTable[i][j] = 0;
                else if (charArray1[i - 1] == charArray2[j - 1])
                    dpTable[i][j] = 1 + dpTable[i - 1][j - 1];
                else
                    dpTable[i][j] = Math.max(dpTable[i - 1][j], dpTable[i][j - 1]);
            }
        }

        return dpTable[size1][size2];
    }


    public static void main( String[] args ) {
        System.out.println(longestCommonSubsequence("abc", "def"));
    }
}
