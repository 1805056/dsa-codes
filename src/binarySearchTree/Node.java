package binarySearchTree;

public class Node {

    private int data;
    private Node parentNode;
    private Node leftNode;
    private Node rightNode;

    public Node( int data, Node parentNode, Node leftNode, Node rightNode ) {
        this.data = data;
        this.parentNode = parentNode;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }

    public int getData() {
        return data;
    }

    public void setData( int data ) {
        this.data = data;
    }

    public Node getParentNode() {
        return parentNode;
    }

    public void setParentNode( Node parentNode ) {
        this.parentNode = parentNode;
    }

    public Node getLeftNode() {
        return leftNode;
    }

    public void setLeftNode( Node leftNode ) {
        this.leftNode = leftNode;
    }

    public Node getRightNode() {
        return rightNode;
    }

    public void setRightNode( Node rightNode ) {
        this.rightNode = rightNode;
    }
}
