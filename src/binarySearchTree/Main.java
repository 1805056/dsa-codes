package binarySearchTree;

public class Main {
    public static void main( String[] args ) {

        BST bst = new BST();

        bst.insert(10);
        bst.insert(5);
        bst.insert(17);
        bst.insert(3);
        bst.insert(7);
        bst.insert(12);
        bst.insert(19);
        bst.insert(1);
        bst.insert(4);

        System.out.println("In order traversal ...............");
        bst.printInOrderTraversal();
        System.out.println("Pre order traversal ..............");
        bst.printPreOrderTraversal();
        System.out.println("Post order traversal ..............");
        bst.printPostOrderTraversal();

        bst.searchInBST(4);

        System.out.println("Minimum in BST is: " + bst.getMinimum());

        bst.deleteBST(17);
        bst.printInOrderTraversal();
    }
}
