package binarySearchTree;

public class BST {

    private Node rootNode;

    //Recursive approach
//    private Node insertNewNode( int data, Node parentNode ) {
//
//        if (parentNode == null) {
//            return new Node(data, null, null);
//        }
//
//        if (data < parentNode.getData())
//            parentNode.setLeftNode(insertNewNode(data, parentNode.getLeftNode()));
//        else if (data > parentNode.getData())
//            parentNode.setRightNode(insertNewNode(data, parentNode.getRightNode()));
//
//        return parentNode;
//    }


    //Iterative implementation requires extra parent node
    private void insertNewNode( int data ) {

        Node newNode = new Node(data, null, null, null);

        if (rootNode == null) {
            rootNode = newNode;
            return;
        }

        Node currentNode = rootNode;
        Node parentNode = null;

        while (currentNode != null) {

            parentNode = currentNode;

            if (data < currentNode.getData()) {
                currentNode = currentNode.getLeftNode();
            } else if (data > currentNode.getData()) {
                currentNode = currentNode.getRightNode();
            }
        }

        if (data < parentNode.getData()) {
            parentNode.setLeftNode(newNode);
        } else {
            parentNode.setRightNode(newNode);
        }

        newNode.setParentNode(parentNode);
    }

    public void insert( int data ) {
        insertNewNode(data);
    }


    private void printInOrderTraversal( Node currentNode ) {

        if (currentNode == null)
            return;

        printInOrderTraversal(currentNode.getLeftNode());
        System.out.println("Data: " + currentNode.getData());
        printInOrderTraversal(currentNode.getRightNode());
    }

    public void printInOrderTraversal() {
        printInOrderTraversal(rootNode);
    }

    private void printPreOrderTraversal( Node currentNode ) {

        if (currentNode == null)
            return;

        System.out.println("Data: " + currentNode.getData());
        printPreOrderTraversal(currentNode.getLeftNode());
        printPreOrderTraversal(currentNode.getRightNode());
    }

    public void printPreOrderTraversal() {
        printPreOrderTraversal(rootNode);
    }

    public void printPostOrderTraversal() {
        printPostOrderTraversal(rootNode);
    }

    private void printPostOrderTraversal( Node currentNode ) {
        if (currentNode == null)
            return;

        printPostOrderTraversal(currentNode.getLeftNode());
        printPostOrderTraversal(currentNode.getRightNode());
        System.out.println("Data: " + currentNode.getData());
    }


    public void searchInBST( int data ) {

        Node searchedValue = searchInBST(data, rootNode);

        if (searchedValue == null) {
            System.out.println("Can not find the given value");
        } else {
            System.out.println("Found the value");
        }
    }

    private Node searchInBST( int data, Node currentNode ) {
        if (currentNode == null || currentNode.getData() == data)
            return currentNode;
        else if (data < currentNode.getData()) {
            return searchInBST(data, currentNode.getLeftNode());
        } else {
            return searchInBST(data, currentNode.getRightNode());
        }
    }

    private Node getMinimumInBST( Node currentNode ) {

        Node tempNode = currentNode;

        while (tempNode.getLeftNode() != null) {
            tempNode = tempNode.getLeftNode();
        }

        return tempNode;
    }

    public int getMinimum() {
        return getMinimumInBST(rootNode).getData();
    }


    //Need to check this
    private void deleteInBST( Node currentNode, int data ) {

        if (currentNode == null)
            return;

        Node targetNode = searchInBST(data, rootNode);

        if (targetNode == null) {
        } else if (targetNode.getLeftNode() == null && targetNode.getRightNode() == null) {
            targetNode = null;
        } else if (targetNode.getRightNode() != null) {
            targetNode.getRightNode().setParentNode(targetNode.getParentNode());
            targetNode = targetNode.getRightNode();
        } else if (targetNode.getLeftNode() != null) {
            targetNode.getLeftNode().setParentNode(targetNode.getParentNode());
            targetNode = targetNode.getLeftNode();
        } else {
            Node tempNode = getMinimumInBST(targetNode.getRightNode());
            tempNode.setParentNode(targetNode.getParentNode());
            targetNode = tempNode;
            deleteInBST(targetNode.getRightNode(), tempNode.getData());
        }

    }

    public void deleteBST( int data ) {
        deleteInBST(rootNode, data);
    }
}
