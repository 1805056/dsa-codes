package greedy;

/**
 * Created by Amimul Ehsan
 * Date: 8/26/2021
 * Time: 9:07 PM
 */

public class ArrayMinMax {

    public static int getMin( int[] array, int low, int high ) {
        if (low >= high)
            return array[low];

        int mid = low + (high - low) / 2;
        int leftSubArrayMax = getMin(array, low, mid);
        int rightSubArrayMax = getMin(array, mid + 1, high);
        return Math.min(leftSubArrayMax, rightSubArrayMax);
    }

    public static int getMax( int[] array, int low, int high ) {
        if (low >= high)
            return array[low];

        int mid = low + (high - low) / 2;
        int leftSubArrayMax = getMax(array, low, mid);
        int rightSubArrayMax = getMax(array, mid + 1, high);
        return Math.max(leftSubArrayMax, rightSubArrayMax);
    }

    public static void main( String[] args ) {
        int[] array = new int[]{7, 2, 11, 13, 9, 18};
        System.out.println(getMax(array, 0, 5));
        System.out.println(getMin(array, 0, 5));
    }
}
