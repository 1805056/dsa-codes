package sortingAlgorithms;

/**
 * Created by Amimul Ehsan
 * Date: 8/17/2021
 * Time: 7:55 PM
 */

public class QuickSort {
    static int partition(int[] array, int low, int high){
        int pivot = array[high];

        int i;
        int j;

        for(i = low - 1, j = low; j<high; j++){
            if(array[j] < pivot){
                i++;

                int temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
        }

        i++;
        int temp = array[j];
        array[j] = array[i];
        array[i] = temp;

        return i;
    }

    static void  quickSort(int[] array, int low, int high){
        if(low >= high)
            return;

        int pivot = partition(array, low, high);
        quickSort(array, low, pivot - 1);
        quickSort(array, pivot + 1, high);
    }


//    static int partition( int[] A, int low, int high ) {
//        int pivot = A[high], i, j, t;
//
//        for (i = low - 1, j = low; j < high; j++) {
//            if (A[j] < pivot) {
//                i++;
//
//                t = A[i];
//                A[i] = A[j];
//                A[j] = t;
//            }
//        }
//
//        i++;
//
//        t = A[high];
//        A[high] = A[i];
//        A[i] = t;
//
//        return i;
//    }
//
//    static void quickSort( int[] A, int low, int high ) {
//        if (low >= high)
//            return;
//
//        int p;
//        p = partition(A, low, high);
//
//        quickSort(A, low, p - 1);
//        quickSort(A, p + 1, high);
//    }

    public static void main( String[] args ) {
        int[] array = {1, 5, 15, 80, 5, 65, 4, 8, 7};

        quickSort(array, 0, 8);

        for (int i : array) {
            System.out.println(i);
        }
    }
}
