package sortingAlgorithms;

/**
 * Created by Amimul Ehsan
 * Date: 8/17/2021
 * Time: 7:55 PM
 */

public class InsertionSort {

    static void insertion( int[] A ) {

        int i, j, item;
        int n = A.length;

        for (i = 1; i < n; i++) {
            item = A[i];

            j = i - 1;

            while (j >= 0 && A[j] > item) {
                A[j + 1] = A[j];
                j--;
            }

            A[j + 1] = item;
        }
    }

    public static void main( String[] args ) {
        int[] array = {1, 5, 15, 80, 5, 65, 4, 8, 7};

        insertion(array);

        for (int i : array) {
            System.out.println(i);
        }
    }
}
