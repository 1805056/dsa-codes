package sortingAlgorithms;

/**
 * Created by Amimul Ehsan
 * Date: 8/17/2021
 * Time: 7:55 PM
 */

public class MergeSort {

    static void mergeSort( int[] A, int low, int high ) {

        if (low >= high)
            return;

        int mid = low + (high - low) / 2;

        mergeSort(A, low, mid);
        mergeSort(A, mid + 1, high);
        merge(A, low, mid, high);
    }

    private static void merge( int[] A, int low, int mid, int high ) {
        int leftSize = mid - low + 1;
        int rightSize = high - mid;

        int[] leftSubArray = new int[leftSize];
        int[] rightSubArray = new int[rightSize];

        if (mid + 1 - low >= 0) System.arraycopy(A, low, leftSubArray, 0, mid + 1 - low);

        if (high + 1 - (mid + 1) >= 0)
            System.arraycopy(A, mid + 1, rightSubArray, 0, high + 1 - (mid + 1));

        int motherArrayIndex = low;
        int leftArrayIndex = 0;
        int rightArrayIndex = 0;

        while (leftArrayIndex < leftSize && rightArrayIndex < rightSize) {
            if (leftSubArray[leftArrayIndex] < rightSubArray[rightArrayIndex]) {
                A[motherArrayIndex++] = leftSubArray[leftArrayIndex++];
            } else {
                A[motherArrayIndex++] = rightSubArray[rightArrayIndex++];
            }
        }

        while (leftArrayIndex < leftSize)
            A[motherArrayIndex++] = leftSubArray[leftArrayIndex++];

        while (rightArrayIndex < rightSize)
            A[motherArrayIndex++] = rightSubArray[rightArrayIndex++];

    }


    public static void main( String[] args ) {
        int[] array = {1, 5, 15, 80, 5, 65, 4, 8, 7};

        mergeSort(array, 0, 8);

        for (int i : array) {
            System.out.println(i);
        }
    }
}
